package edu.udmercy.zodiacfragments

data class HoroscopeItem(
    val credit: String,
    val date: String,
    val horoscope: String,
    val meta: Meta,
    val sunsign: String
)