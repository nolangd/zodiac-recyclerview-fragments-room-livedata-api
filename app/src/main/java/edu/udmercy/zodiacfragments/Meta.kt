package edu.udmercy.zodiacfragments

data class Meta(
    val intensity: String,
    val keywords: String,
    val mood: String
)