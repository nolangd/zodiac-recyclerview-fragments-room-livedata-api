package edu.udmercy.zodiacfragments

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

private const val TAG = "AstrologerFetchr"

class AstrologerFetchr {

    private val astrologerApi: AstrologerApi

    init {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("http://www.sandipbgt.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        astrologerApi = retrofit.create(AstrologerApi::class.java)
    }

    fun fetchHoroscope(sign : String): LiveData<String> {
        val responseLiveData: MutableLiveData<String> = MutableLiveData()
        val astrologerRequest: Call<HoroscopeItem> = astrologerApi.fetchHoroscope(sign)
        var data: HoroscopeItem

        astrologerRequest.enqueue(object : Callback<HoroscopeItem> {

            override fun onFailure(call: Call<HoroscopeItem>, t: Throwable) {
                Log.e(TAG, "Failed to fetch horoscope", t)
            }

            override fun onResponse(
                call: Call<HoroscopeItem>,
                response: Response<HoroscopeItem>
            ) {
                Log.d(TAG, "Response received")
                data = response.body()!!
                responseLiveData.value = data.horoscope
                Log.d(TAG, data.horoscope)

            }
        })

        return responseLiveData
    }
}



