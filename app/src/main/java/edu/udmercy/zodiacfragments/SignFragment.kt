package edu.udmercy.zodiacfragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory

private const val TAG = "SignFragment"
private const val ARG_SIGN_ID = "sign_id"


class SignFragment : Fragment() {

    private lateinit var sign: Sign
    private lateinit var signName : TextView
    private lateinit var signDescription : TextView
    private lateinit var signMonth : TextView
    private lateinit var signSymbol : TextView
    private lateinit var signDaily : TextView

    private val signViewModel: SignViewModel by lazy {
        ViewModelProviders.of(this).get(SignViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val signId = arguments?.getSerializable(ARG_SIGN_ID) as Int
        Log.d(TAG, "args bundle sign ID: $signId")
        signViewModel.loadSign(signId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        signViewModel.signsLiveData.observe(
            viewLifecycleOwner,
            Observer { sign ->
                sign?.let {
                    this.sign = sign
                    updateUI()
                }
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_sign, container, false)
        signName = view.findViewById(R.id.name) as TextView
        signDescription = view.findViewById(R.id.description) as TextView
        signMonth = view.findViewById(R.id.month) as TextView
        signSymbol = view.findViewById(R.id.symbol) as TextView
        signDaily = view.findViewById(R.id.daily) as TextView

        return view
    }

    private fun updateUI() {
        signName.text = (sign.name)
        signDescription.text =(sign.description)
        signMonth.text = (sign.month)
        signSymbol.text = (sign.symbol)

        val astrologerLiveData: LiveData<String> = AstrologerFetchr().fetchHoroscope(sign.name.toLowerCase())
        astrologerLiveData.observe(
            this,
            Observer { responseString ->
                Log.d(TAG, "Response received: $responseString")
                signDaily.text = responseString

            })

    }

    companion object {

        fun newInstance(signId: Int): SignFragment {
            val args = Bundle().apply {
                putSerializable(ARG_SIGN_ID, signId)
            }
            return SignFragment().apply {
                arguments = args
            }
        }
    }
}

