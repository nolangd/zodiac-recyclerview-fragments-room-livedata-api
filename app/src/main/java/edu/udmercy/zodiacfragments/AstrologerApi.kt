package edu.udmercy.zodiacfragments

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path


interface AstrologerApi {

        @GET("/theastrologer/api/horoscope/{sunsign}/today")
        fun fetchHoroscope(@Path("sunsign") sunsign:String): Call<HoroscopeItem>
}


